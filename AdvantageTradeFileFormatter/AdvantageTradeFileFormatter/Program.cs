﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace AdvantageTradeFileFormatter
{
    class Program
    {

        static void getNamePath(string filePath, out string path, out string fileName)
        {
            int index = filePath.LastIndexOf(@"\");
            fileName = filePath.Substring(index + 1, filePath.Length - index - 1);
            path = filePath.Substring(0, index);
        }

        static string determineFirmID(string trade)
        { // trade sample: 737A9591,ADVANTAGE,VSOUL,FV,U3,B,1.000000,CBOT,121.14843750,,
            char[] separator = ",".ToCharArray();
            string[] parts = trade.Split(separator);
            if ((parts[3] == "KC") || (parts[3] == "RL") || (parts[3] == "CC") || (parts[3] == "CT") || (parts[3] == "SB") || (parts[3] == "OJ")
                || parts[7] == "COMEX" || parts[7] == "NYMEX")
            {
                return "132";
            }
            else if (parts[3] == "LCO" || parts[3] == "LGO" || parts[3] == "WTI")
            {
                if (parts[0] == "2000VK12")
                {
                    return "FIM";
                }
                else
                {
                    return "FIU";
                }
            }
            else
            {
                return "714";
            }
            //if ((parts[3] == "KC") || (parts[3] == "RL") || (parts[3] == "CC") || (parts[3] == "CT") || (parts[3] == "SB") || (parts[3] == "OJ") || (parts[3] == "LCO"))
            //{
            //    return "FIM";
            //}
            //else 
            //{
            //    return "132";
            //}
        }

        // updated on 20130827 by Regant
        // begin
        static string updateForLCO(string line)
        {
            char[] separator = ",".ToCharArray();
            string[] parts = line.Split(separator);
            if (parts[3] == "LCO" || parts[3] == "LGO" || parts[3] == "WTI")
            {
                if (parts[0].StartsWith("737A"))
                {
                    parts[1] = "ACE";
                }
                parts[7] = "IPE";
            }
            string updatedLine = "";
            for (int i = 0; i < parts.Length -1; ++i)
            {
                updatedLine += parts[i] + ",";
            }
            updatedLine += parts[parts.Length - 1];
            return updatedLine;
        }
        // end

        // modified by Regant 20131002 begin
        static void replaceClearingAccount_LT(ref string line)
        {
            int pos = line.IndexOf(",");
            string clearingAccount = line.Substring(0, pos);
            if (clearingAccount.EndsWith("LT"))
            {
                line = line.Replace(clearingAccount, clearingAccount.Substring(0, clearingAccount.Length - 2));
            }
        }
        // end

        static void modifyFile(string file)
        {  
            string lines = "", line;
            string path, filename;
            getNamePath(file, out path, out filename);

            File.Copy(file, path + @"\backup\" + filename, true);

            using (StreamReader reader = new StreamReader(file))
            {
                while ((line = reader.ReadLine()) != null)
                {
                    // modified by Regant 20131002 begin
                    replaceClearingAccount_LT(ref line);
                    // end                    
                    if (line.Contains("0VK12_"))
                    {
                        line = line.Replace("0VK12_", "2000VK12");
                        line = line.Replace("ADVANTAGE", determineFirmID(line));
                    }
                    else if (line.Contains("007-99275"))
                    {
                        line = line.Replace("007-99275", "00799275");
                        line = line.Replace("ADVANTAGE", determineFirmID(line));
                    }
                    // updated on 20130827 by Regant
                    // begin
                    if (line.Contains("LCO") || line.Contains("LGO") || line.Contains("WTI"))
                    {
                        line = updateForLCO(line);
                    }
                    // end

                    lines += line + "\r\n";
                }
            }
            using (StreamWriter writer = new StreamWriter(file))
            {
                writer.Write(lines);
            }
        }

        static void Main(string[] args)
        {
            // input parameters: 
            // 1. tradedate 
            // 2. path
            try
            {
                string tradeDate = args[0];
                string path = @"C:\TradeBlotter\Allocated Trades\Advantage\";
                if (args.Length >= 2)
                {
                    path = args[1];
                }

                string[] files = Directory.GetFiles(path);
                string filePattern = "vegasoul_" + tradeDate;
                foreach (string file in files)
                {
                    // sample trade file name: vegasoul_20130523_adv_alloc_APSALL
                    if (file.Contains(filePattern))
                    {
                        modifyFile(file);
                    }
                }
            }
            catch (Exception ex)
            {
                using (StreamWriter writer = new StreamWriter("log.txt", true))
                {
                    writer.WriteLine(DateTime.Now.ToString("yyyyMMdd HH:mm:ss: ") + ex.Message + "\r\n" + ex.StackTrace);
                }
            }
        }
    }
}
